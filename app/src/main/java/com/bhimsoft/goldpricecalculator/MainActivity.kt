package com.bhimsoft.goldpricecalculator

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import com.bhimsoft.goldpricecalculator.communication.ApiService
import com.bhimsoft.goldpricecalculator.communication.models.ResponseV2
import com.bhimsoft.goldpricecalculator.utility.Utils
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainActivity : AppCompatActivity() {
    private val apiService by lazy {
        ApiService.create()
    }
    private var disposable: Disposable? = null
    private var oneGramPrice: Float = 0.0f
    /*
     10K=41.67% of Pure Gold
     14K=58.33% of Pure Gold
     18K=75% of Pure Gold
     */
    private val percent10K: Float = 41.67f
    private val percent14K: Float = 58.33f
    private val percent18K: Float = 75.00f
    private var selectedTypePercentage: Float = percent10K
    private var noInternetView: RelativeLayout? = null

    //http://goldpricez.com/api/rates/currency/usd/measure/gram?X-API-KEY=d19dd070b79f9d8bcf767af788f89fe9d19dd070
    //Free  http://goldpricez.com/about/api


    @Suppress("UNUSED_ANONYMOUS_PARAMETER")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPreferences = getSharedPreferences("local_tini_db", Context.MODE_PRIVATE)
        val lastPriceOunce = sharedPreferences.getFloat("last_price_ounce", 0.0f)
        val lastPriceGrams = sharedPreferences.getFloat("last_price_grams", 0.0f)
        setUpPrices(lastPriceOunce, lastPriceGrams)
        /*
        ounceInGram=1/28.3495===Gram
        so 1 gram price= $coming from site x ounceInGram
         */
        val params = HashMap<String, String>()
        params["X-API-KEY"] = "d19dd070b79f9d8bcf767af788f89fe9d19dd070"
//        params["api_key"] = "tH3QVsswg4uF5fn7U1t3"
//        params["rows"] = "1"
        loadData(params, lastPriceGrams == 0.0f)
        val edtWeight = findViewById<EditText>(R.id.weight)
        edtWeight.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.isNullOrBlank()) {
                    calculateGoldPrice(0.0f)
                    return
                }
                calculateGoldPrice(s.toString().toFloat())
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        findViewById<RadioGroup>(R.id.gold_type).setOnCheckedChangeListener({ group, checkedId ->
            selectedTypePercentage = when (checkedId) {
                R.id._14k -> percent14K
                R.id._18k -> percent18K
                else -> percent10K
            }
            val text = edtWeight.text
            if (!text.isNullOrBlank()) {
                calculateGoldPrice(text.toString().toFloat())
            }
        })
        noInternetView = findViewById<RelativeLayout>(R.id.no_internet_view)
        findViewById<TextView>(R.id.powered_by).setOnClickListener({ view ->
            val uris = Uri.parse(getString(R.string.external_url))
            val intents = Intent(Intent.ACTION_VIEW, uris)
            startActivity(intents)
        })
    }

    override fun onResume() {
        super.onResume()
        if (!Utils.isInternet(baseContext) && oneGramPrice == 0.0f) {
            noInternetView?.visibility = View.VISIBLE
        } else {
            noInternetView?.visibility = View.GONE
            if (oneGramPrice == 0.0f) {
                val params = HashMap<String, String>()
//                params["api_key"] = "tH3QVsswg4uF5fn7U1t3"
//                params["rows"] = "1"
                params["X-API-KEY"] = "d19dd070b79f9d8bcf767af788f89fe9d19dd070"
                loadData(params, true)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (disposable?.isDisposed!!) {
            disposable?.dispose()
        }
    }

    private fun loadData(params: HashMap<String, String>, showLoading: Boolean) {
        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        if (showLoading) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.INVISIBLE
        }
        disposable =
                apiService.getGoldPriceV2(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    progressBar.visibility = View.INVISIBLE
                                    showResult(result)

                                },
                                { error ->
                                    progressBar.visibility = View.INVISIBLE
                                    Log.e(">>>Error", error.message)
                                    if (!Utils.isInternet(baseContext) && showLoading) {
                                        noInternetView?.visibility = View.VISIBLE
                                    } else {
                                        noInternetView?.visibility = View.GONE
                                    }
                                    if (showLoading) {
                                        Toast.makeText(applicationContext, R.string.error_issue_occur_while_fetch_data, Toast.LENGTH_SHORT).show()
                                    }
                                }
                        )
    }

    private fun setUpPrices(lastPriceOunce: Float, lastPriceGram: Float) {
        oneGramPrice = lastPriceGram
//        oneGramPrice = (1.0f / 31.1034768f) * lastPrice
        findViewById<TextView>(R.id.one_ounce_price).text = String.format(Locale.getDefault(), "*One Ounce Price=\$%.4f", (lastPriceOunce))
        findViewById<TextView>(R.id.one_gram_price).text = String.format(Locale.getDefault(), "*One Gram Price (pure)=\$%.4f", (oneGramPrice))
    }

    private fun showResult(response: String) {
        val json = response.replace("\\", "")
        Log.d(">>>OnSuccess", json)
        val responseV2: ResponseV2 = GsonBuilder().create().fromJson(json, ResponseV2::class.java)
        Log.d(">>>OnSuccess", json)
        val sharedPreferences = getSharedPreferences("local_tini_db", Context.MODE_PRIVATE)
//        val priceInOunce = response.dataset!!.getPriceInOunce()
        sharedPreferences.edit().putFloat("last_price_ounce", responseV2.gram_in_usd).apply()
        setUpPrices(responseV2.ounce_price_usd.toFloat(), responseV2.gram_in_usd)
    }

    private fun calculateGoldPrice(weight: Float) {
        Log.d(">>>CalculatePrice", selectedTypePercentage.toString())
        val tvTotalValue = findViewById<TextView>(R.id.total_value)
        tvTotalValue.text = String.format(Locale.getDefault(), " \$%.3f", (((selectedTypePercentage * oneGramPrice) / 100) * weight))
    }
}
