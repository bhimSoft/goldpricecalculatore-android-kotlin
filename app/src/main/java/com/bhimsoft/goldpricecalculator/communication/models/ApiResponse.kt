package com.bhimsoft.goldpricecalculator.communication.models

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 08/05/2018.
 */
class ApiResponse {
    var dataset: DataSet? = null

    override fun toString(): String {
        return dataset!!.toString()
    }
}