package com.bhimsoft.goldpricecalculator.communication

import com.bhimsoft.goldpricecalculator.communication.models.ApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 07/05/2018.
 */
interface Api {
    //    https://www.quandl.com/api/v3/datasets/WGC/GOLD_DAILY_USD?api_key=tH3QVsswg4uF5fn7U1t3&rows=1&type=10k
//    @GET("WGC/GOLD_DAILY_USD.json")
//    fun getGoldPrice(@QueryMap params: HashMap<String, String>):
//            Observable<ApiResponse>

    @GET("rates/currency/usd/measure/gram")
    fun getGoldPriceV2(@QueryMap params: HashMap<String, String>):
            Observable<String>
}