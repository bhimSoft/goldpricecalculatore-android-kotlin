package com.bhimsoft.goldpricecalculator.communication.models

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 09/05/2018.
 */
class ResponseV2 {
    var ounce_price_usd: String = "0.0f"
    var gmt_ounce_price_usd_updated: String? = null
    var gram_in_usd: Float = 0.0f
}