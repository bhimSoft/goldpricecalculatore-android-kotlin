package com.bhimsoft.goldpricecalculator.communication.models

/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 07/05/2018.
 */
class DataSet {
    var id: Long = 0
    var name: String? = null
    var data: Array<Array<Any>>? = null

    override fun toString(): String {
        return id.toString() + " , " + name + " Price=" + getPriceInOunce()
    }

    fun getPriceInOunce(): Float {
        return data!![0][1].toString().toFloat()
    }
}