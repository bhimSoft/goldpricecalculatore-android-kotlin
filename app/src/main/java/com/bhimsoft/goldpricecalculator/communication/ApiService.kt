package com.bhimsoft.goldpricecalculator.communication

import com.bhimsoft.goldpricecalculator.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by m.imran
 * Senior Software Engineer at
 * BhimSoft on 07/05/2018.
 */
class ApiService {
    companion object {
        fun create(): Api {
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create(gson))
                    .baseUrl(BuildConfig.END_POINT)
                    .client(getOkHttpClient())
                    .build()
            return retrofit.create(Api::class.java)
        }

        fun getOkHttpClient(): OkHttpClient {
            val httpClient = OkHttpClient.Builder()
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.interceptors().add(interceptor)
            return httpClient.build()
        }
    }

}