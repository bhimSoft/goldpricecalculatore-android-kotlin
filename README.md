# Gold Price Calculator
Gold Price Calculator is Open source project developed by **Muhammad Imran Senior Android Developer at BhimSoft** in Kotlin
#Used libraries are:

1. Retrofit
2. RxJava
3. OkHttp Client
4. DataBinding



This is an opensource project for Demo's, students who want to learn Kotlin, RetroFit, RxJava etc.


# Contact Us
**Muhammad Imran**  
**Email: bhimsoft1@gmail.com**  
**Cell: +923216855879 **  
**Skype: imran_1664**